"use strict";

var finance = {};

// contains a string list of all the modules in the app
finance.modules = [];
// centralized container for data we've obtained from the API. Used for syncronization between modules.
finance.data = {};

finance.setup = function () {
    var self = this;

    $.extend({
        put: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "PUT");
        },
        delete_: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "DELETE");
        }
    });

    $.ajaxSetup({xhrFields: {withCredentials: true}});

    $(document).ajaxError(function () {
        utils.danger(
            "There was an error processing your request. " +
                "Please reload the page and try again. "
        );
    });

    this.$messages = $("#messages");
    this.$login = $("#login");

    /// totals module stuff ///
    this._$totals = $("#totals");
    this._$total = $("#total");
    this._$totalDate = this._$totals.find("tr:first-child td:first-child span");
    this._totals = {};
    this._total = 0;

    this.baseUrl = window.location.protocol + "//" + window.location.host;
    /// The API URL. Used in all AJAX calls. ///
    this.url = window.location.protocol + "//api." +
        window.location.host.split(".").slice(1).join(".");
    ///--------------------------------------///
    //this.url = "http://127.0.0.1:5000";

    $.get(this.url + "/current_user/", function (data) {
        if (data.success) {
            self.data.user = data.user;
            self._launch();
        } else {
            self.loginForm = new utils.Form({
                email: utils.Form.gen.input("Email", "lana.kane@isis.gov"),
                password: utils.Form.gen.password("Password", "••••••••")
            }, {
                url: self.url + "/users/login/",
                successAction: self._launch.bind(self)
            });
            self.$login.find(".panel-body").append(self.loginForm.render());
        }
    });
};

finance.updateTotal = function (module, cents, seconds) {
    var now = parseInt(new Date().getTime() / 1000, 10), red = "color: red;";

    this._total -= this._totals[module];
    this._totals[module] = cents;
    this._total += this._totals[module];

    this._$totals.find("td[module=" + module + "]").text(utils.toMoney(cents));
    this._$total
        .text(utils.toMoney(this._total))
        .attr("style", this._total < 0 ? red : "");

    if (seconds) {
        this._$totalDate.text(utils.date(seconds))
            .attr("style", seconds < now ? red : "");
    }
};

finance._launch = function () {
    var self = this;

    $("body").removeClass("loginPage");
    this.$login.add($("section, nav")).toggleClass("hidden");
    $("#logout").click(function () {
        $.get(self.url + "/users/logout/", function () {
            location.reload();
        });
    });

    this.modules.forEach(function (module) {
        self[module].setup();
        self._totals[module] = 0;

        if (window.location.hash.substr(1) === module) {
            $("nav a[href=#" + module + "]").click();
        }
    });

    $("nav a").click(function () {
        window.location.hash = this.getAttribute("href").substr(1);
    });
};
