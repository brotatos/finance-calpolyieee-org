"use strict";

finance.Pagination = function ($container, $buttonsContainer, elements) {
    var self = this, i, $btn, makeBtn = function (str) {
        return $("<li><a href=\"javascript: void 0;\">" +
                 (str && "<i class=\"fa fa-angle-" + str + "\"></i>" || "") +
                 "</a></li>");
    };

    this.numPages = Math.ceil(elements.length / finance.Pagination.PER_PAGE);
    this.currPage = 0;
    this.pages = [];
    this.buttons = [];
    this.$container = $container;
    this.$back = makeBtn("left");
    this.$forward = makeBtn("right");
    this.$start = makeBtn("double-left");
    this.$end = makeBtn("double-right");
    this.$buttons = $("<ul class=\"pagination\">");

    this.$buttons.append(this.$start, this.$back);
    for (i = 0; i < this.numPages; i++) {
        this.pages.push([]);

        $btn = makeBtn();
        $btn.children().text(i + 1);
        this.buttons.push($btn);
        this.$buttons.append($btn);
        $btn.click(function () {
            var ndx = parseInt(this.childNodes[0].innerHTML, 10);

            self.btnClick(true, function () { return ndx - 1; });
        });
    }
    this.$buttons.append(this.$forward, this.$end);

    elements.forEach(function (element, ndx) {
        this.pages[parseInt(ndx / finance.Pagination.PER_PAGE, 10)].push(element);
    }, this);

    this.$back.click(function () {
        self.btnClick(self.currPage > 0, function (n) { return n - 1; });
    });

    this.$forward.click(function () {
        self.btnClick(self.currPage < self.numPages - 1,
                      function (n) { return n + 1; });
    });

    this.$start.click(function () {
        self.btnClick(self.currPage > 0, function () { return 0; });
    });

    this.$start.click(function () {
        self.btnClick(self.currPage > 0, function () { return 0; });
    });

    this.$end.click(function () {
        self.btnClick(self.currPage < self.numPages - 1,
                      function () { return self.numPages - 1; });
    });

    if (elements.length === 0) {
        this.$buttons.children().addClass("disabled");
    } else {
        this.buttons[0].click();
    }

    $buttonsContainer.append(this.$buttons);
};

finance.Pagination.prototype.btnClick = function (condition, operation) {
    if (condition) {
        this.buttons[this.currPage].removeClass("active");
        this.currPage = operation(this.currPage);
        this.buttons[this.currPage].addClass("active");
        this.$container.children().detach();
        this.$container.append(this.pages[this.currPage]);
        this.$back.attr("class", this.currPage > 0 ? "" : "disabled");
        this.$start.attr("class", this.currPage > 0 ? "" : "disabled");
        this.$forward.attr("class", this.currPage < this.numPages - 1 ?
                           "" : "disabled");
        this.$end.attr("class", this.currPage < this.numPages - 1 ?
                       "" : "disabled");
    }
};

finance.Pagination.prototype.destroy = function () {
    this.$buttons.remove();
    this.$container.children().remove();
};


finance.Pagination.PER_PAGE = 10;
