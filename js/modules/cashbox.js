"use strict";

finance.modules.push("cashbox");
finance.cashbox = {};

finance.cashbox.setup = function () {
    var self = this, url = finance.url + "/cashbox_totals/";
    
    this.$cashbox = $("#cashbox");
    this.$cashboxTable = this.$cashbox.find("tbody");
    this.$cbNew = this.$cashbox.find("div.btn-group li:first-child a");
    this.total = 0;

    this.$cbNew.click(function () {
        var modal = new utils.ModalAlert();

        $("body").append(modal.render({
            title: "New Cashbox Total",
            body: self.cbForm.render(),
            cancel: "OK"
        }));
        modal.show();
    });

    this.cbForm = new utils.Form({
        amount: utils.Form.gen.money("Amount")
    }, {
        url: finance.url + "/cashbox_totals/",
        successAction: function (data, json) {
            json.id = data.id;
            json.date = (new Date()).getTime() / 1000;
            self.cbTotals.unshift(json);
            self.show();
        }
    });

    this.cbPagination = new finance.Pagination(
        this.$cashboxTable,
        this.$cashbox.find("div.panel-footer"),
        []
    );

    $.get(url, {sort: "-date"}, utils.orAlert(function (data) {
        self.cbTotals = data.cashbox_totals;
        self.show();
    }));
};

finance.cashbox._mapCbTotal = function (cbTotal) {
    var self = this, $delete = utils.btn("Delete", "danger").addClass("btn-sm"),
        url = finance.url + "/cashbox_totals/" + cbTotal.id + "/";

    $delete.click(utils.doubleCheck("delete this cashbox total?", function () {
        $.delete_(url, utils.orAlert(function () {
            utils.info({
                msg: "Item successfully deleted.",
                dismiss: true
            });
            self.cbTotals.splice(self.cbTotals.indexOf(cbTotal), 1);
            self.show();
        }));
    }));

    return utils.toTr(cbTotal.id, utils.toMoney(cbTotal.amount),
        utils.date(cbTotal.date), $delete);
};

finance.cashbox.show = function () {
    this.total = this.cbTotals.length && this.cbTotals[0].amount || 0;
    this.cbPagination.destroy();
    this.$cashboxTable.empty();
    this.cbPagination = new finance.Pagination(
        this.$cashboxTable,
        this.$cashbox.find("v.panel-footer"),
        this.cbTotals.map(this._mapCbTotal, this)
    );
    finance.updateTotal("cashbox", this.total);
};
