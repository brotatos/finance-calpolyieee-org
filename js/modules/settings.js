"use strict";

finance.modules.push("settings");
finance.settings = {};

finance.settings.setup = function () {
    var self = this;

    $.get(finance.url + "/debug/", utils.orAlert(function (data) {
        if (data.debug) {
            $("nav > ul > li:last-child span.badge").removeClass("hidden");
        }
    }));

    ///access panel setup///
    this.$accessPanel = $("#accessPanel");
    this.$accessTable = this.$accessPanel.find("tbody");
    this.$newUserBtn = this.$accessPanel.find("ul.dropdown-menu > li:first-child");
    this.newUserForm = new utils.Form({
        user_id: utils.Form.gen.select("User"),
        permissions: utils.Form.gen.input("Permissions")
    }, {
        url: finance.url + "/settings/",
        successAction: function (data, json) {
            json.id = data.id;
            self.settings.push(json);
            self.$accessTable.empty().append(
                self.settings.map(self._mapSettings, self)
            );
        }
    });

    ///info panel setup///
    this.$infoPanel = $("div#infoPanel");
    this.$name = this.$infoPanel.find("tr:nth-child(1) > td:nth-child(2)");
    this.$pass = this.$infoPanel.find("tr:nth-child(2) > td:nth-child(2)");
    this.$perms = this.$infoPanel.find("tr:nth-child(3) > td:nth-child(2)");
    this.$changePassBtn = this.$infoPanel.find("tr:nth-child(2) a.btn");
    this.changePassForm = new utils.Form({
        current: utils.Form.gen.password("Current Password"),
        "new": utils.Form.gen.password("New Password"),
        confirm: utils.Form.gen.password("Confirm Password")
    }, {
        url: finance.url + "/current_user/change_password/"
    });

    $.get(finance.url + "/current_user/", utils.orAlert(function (data) {
        self.$name.text(data.user.name);
        self.$perms.text(data.user.permissions);
        if (data.user.permissions.indexOf("settings:rw") !== -1) {
            self.setupAccessPanel();
        }
    }));

    this.$changePassBtn.click(function () {
        var $form = self.changePassForm.render().prop("id", "changePassword");

        utils.quickModal("Change Password", $form);
    });
};

finance.settings.setupAccessPanel = function () {
    var self = this, promises = [];

    this.$accessPanel.removeClass("hidden");

    promises.push($.get(
        finance.url + "/settings/",
        utils.orAlert(function (data) { self.settings = data.settings; })
    ));

    promises.push($.get(
        finance.url + "/users/",
        utils.orAlert(function (data) { self.users = data.users; })
    ));

    $.when.apply(null, promises).then(function () {
        self.$accessTable.empty().append(
            self.settings.map(self._mapSettings, self)
        );
        self.$newUserBtn.click(function () {
            utils.quickModal("Add User to Application", self.newUserForm.render());
        });
        self.newUserForm.skel.user_id.fill(self.users.map(function (user) {
            return {text: user.name, val: user.id};
        }));
        self.$accessPanel.removeClass("loading");
    });
};

finance.settings._mapSettings = function (userPerms) {
    var self = this;
    var $editBtn = utils.btn("Edit");
    var editForm = new utils.Form({
        user_id: utils.Form.gen.p("User", userPerms.user_name),
        permissions: utils.Form.gen.input("Permissions")
    }, {
        url: finance.url + "/settings/" + userPerms.id + "/",
        edit: true,
        successAction: function (data, json) {
            var ndx = self.settings.indexOf(userPerms);
            self.settings[ndx].permissions = json.permissions;
            self.$accessTable.find("tr:nth-child(" + (ndx + 1) +
                                   ") td:nth-child(3)")
                .text(json.permissions);
        }
    });

    $editBtn.click(function () {
        editForm.skel.permissions.val(userPerms.permissions);
        utils.quickModal("Edit User Permissions", editForm.render());
    });

    return utils.toTr(userPerms.user_id, userPerms.user_name,
                      userPerms.permissions, $editBtn);
};
