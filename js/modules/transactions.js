"use strict";

finance.modules.push("transactions");
finance.transactions = {};

finance.transactions.setup = function () {
    var self = this, bcUrl = finance.url + "/budget_categories/",
        tUrl = finance.url + "/transactions/";

    this.promises = [];
    // empty array of the next transactions
    this.next = [];
    this.total = 0;

    /// timeline panel ///
    this.now = parseInt(new Date().getTime() / 1000, 10);
    this.$timeline = $("#timelinePanel");
    this.$timelineTable = this.$timeline.find("tbody");
    this.$dateSel = this.$timeline.find(".panel-body input");
    this.$dateBtn = this.$timeline.find(".panel-body a.btn");
    this.$trPrev = this.$timeline.find(".pager li:nth-of-type(2)");
    this.$trDate = this.$timeline.find(".pager li:nth-of-type(3) p");
    this.$trNext = this.$timeline.find(".pager li:nth-of-type(4)");
    this.$trLast = this.$timeline.find(".pager li:nth-of-type(5)");

    /// transactions panel ///
    this.$transactions = $("#transactionsPanel");
    this.$transactionsTable = this.$transactions.find("tbody");
    this.$trNew = this.$transactions.find("div.btn-group li:first-child a");
    this.$bcNew = this.$transactions.find("div.btn-group li:nth-child(2) a");

    this.$trNew.click(function () {
        var modal = new utils.ModalAlert();

        $("body").append(modal.render({
            title: "New Transaction",
            body: self.trForm.render(),
            cancel: "OK"
        }));
        modal.show();
    });

    this.trForm = new utils.Form({
        budget_category: utils.Form.gen.select("Category"),
        name: utils.Form.gen.input("Name"),
        date: utils.Form.gen.date("Date"),
        amount: utils.Form.gen.money("Amount")
    }, {
        url: tUrl,
        successAction: function (data, json) {
            json.id = data.id;
            self.transactions.filter(function (category) {
                return category.name === json.budget_category;
            })[0].transactions.push(json);
            self.show();
        }
    });

    this.trForm.skel.date.val(this.now);

    this.$bcNew.click(function () {
        var modal = new utils.ModalAlert();

        $("body").append(modal.render({
            title: "New Budget Category",
            body: self.bcForm.render(),
            cancel: "OK"
        }));
        modal.show();
    });

    this.bcForm = new utils.Form({
        name: utils.Form.gen.input("Name"),
        type: utils.Form.gen.select("Type")
    }, {
        url: finance.url + "/budget_categories/",
        successAction: function (data, json) {
            json.transactions = [];
            self.transactions.push(json);
            self.show();
        }
    });

    this.bcForm.skel.type.fill(["TRACKING", "NON_TRACKING"], true);

    this.$timeline.find(".panel-heading > a").click(function () {
        self.$timeline.toggleClass("collapsed");
    });

    this.$trDate.text(utils.date(this.now));

    this.$dateBtn.click(function () {
        self.now = parseInt(new Date(self.$dateSel.val()).getTime() / 1000, 10);

        $.get(tUrl, {
            filter: "date<" + (self.now || 0),
            sort: "date"
        }, utils.orAlert(function (data) {
            self.transactions = data.transactions;
            self.next = data.next;
            self.show();
        }));
    });

    this.promises.push($.get(bcUrl, utils.orAlert(function (data) {
        self.budget_categories = data.budget_categories;
        self.trForm.skel.budget_category.fill(
            data.budget_categories.map(function (bc) {
                return bc.name;
            }), true)
    })));

    this.promises.push($.get(tUrl, utils.orAlert(function (data) {
        self.transactions = data.transactions;
    })));

    $.when.apply(null, this.promises).then(this.show.bind(this));
};

finance.transactions._next = function () {
    var tr = this.next.shift();

    this.now = parseInt(tr.date, 10);
    this.transactions.push(tr);
    this.show();
};

finance.transactions._prev = function () {
    this.next.unshift(this.transactions.pop());
    if (this.transactions.length) {
        this.now = parseInt(this.transactions[
            this.transactions.length - 1].date, 10);
    }
    this.show();
};

finance.transactions._last = function () {
    var self = this;

    $.get(finance.url + "/transactions/", utils.orAlert(function (data) {
        self.now = new Date().getTime() / 1000;
        self.transactions = data.transactions;
        self.next = [];
        self.show();
    }));
};

finance.transactions._bcMap = function (category) {
    var self = this, $tr;
    var $toggle = $("<i class=\"fa fa-angle-right\">");
    var $row = $("<tr class=\"hidden innerRow\">");
    var $delete = utils.btn("Delete Category", "danger").addClass("btn-sm");
    var catTotal = this._budget[category.name].reduce(function (sum, tr) {
        return sum += tr.amount;
    }, 0);

    this.total += category.type === "TRACKING" ? catTotal : 0;

    $tr = utils.toTr(
        $toggle,
        category.name,
        this._budget[category.name].length,
        category.type === "TRACKING" ? "&#10004;" : "",
        utils.toMoney(catTotal),
        $delete
    );

    $toggle.parent().click(function () {
        $toggle.toggleClass("fa-angle-right");
        $toggle.toggleClass("fa-angle-down");
        $tr.after($row);
        $row.toggleClass("hidden");
    });

    $row.append("<td></td>", $("<td colspan=\"5\">").append(
        self._makeTransactionsTable(this._budget[category.name])
    ));

    $delete.click(utils.doubleCheck("delete this budget category?", function () {
        var url = finance.url + "/budget_categories/" + category.id + "/";

        $.delete_(url, utils.orAlert(function () {
            utils.info({msg: "Delete Successful", dismiss: true});
            self.budget_categories
                .splice(self.budget_categories.indexOf(category), 1);
            delete self._budget[category.name];
            self.show();
        }));
    }));

    return $tr;
};

finance.transactions._makeTransactionsTable = function (transactions) {
    var self = this, $table = $("#transactionsInner").clone();

    $table.attr("id", "").removeClass("hidden");
    new finance.Pagination(
        $table.children("tbody"),
        $table.find("td[colspan=5]"),
        transactions.map(function (tr) {
            var $delete = utils.btn("Delete", "danger").addClass("btn-sm");
            var url = finance.url + "/transactions/" + tr.id + "/";

            $delete.click(utils.doubleCheck("delete this transaction?", function () {
                $.delete_(url, utils.orAlert(function () {
                    utils.info({msg: "Transaction deleted.", dismiss: true});
                    transactions.splice(transactions.indexOf(tr), 1);
                    self.show();
                }));
            }));

            return utils.toTr(tr.id, tr.name, utils.toMoney(tr.amount),
                              utils.date(tr.date), $delete);
        })
    );

    return $table;
};

finance.transactions.show = function () {
    var lastTr, nextTr;

    this.total = 0;
    this._budget = {};

    // Group all transactions by budget category
    this.budget_categories.forEach(function (category) {
        this._budget[category.name] = [];
    }, this);

    this.transactions.forEach(function (tr) {
        this._budget[tr.budget_category].push(tr);
    }, this);

    /// Stuff that actually updates the dom ///
    if (!this.next.length) {
        this.$trPrev.unbind().click(this._prev.bind(this)).attr("class", "");
        this.$trNext.unbind().addClass("disabled");
        this.$trLast.unbind().addClass("disabled");
    } else if (!this.transactions.length) {
        this.$trPrev.unbind().addClass("disabled");
        this.$trNext.unbind().click(this._next.bind(this)).removeClass("disabled");
        this.$trLast.unbind().click(this._last.bind(this)).removeClass("disabled");
    } else {
        this.$trPrev.unbind().click(this._prev.bind(this)).removeClass("disabled");
        this.$trNext.unbind().click(this._next.bind(this)).removeClass("disabled");
        this.$trLast.unbind().click(this._last.bind(this)).removeClass("disabled");
    }

    lastTr = this.transactions[this.transactions.length - 1];
    nextTr = this.next[0];

    this.$timelineTable.empty();
    if (lastTr) {
        this.$timelineTable.append(utils.toTr(
            "Most Recent Transaction",
            lastTr.id,
            lastTr.budget_category,
            lastTr.name,
            utils.toMoney(lastTr.amount),
            utils.date(lastTr.date)
        ));
    } else {
        this.$timelineTable.append("<tr><td>Most Recent Transaction</td>" +
                                   "<td colspan=\"5\"><em>At first " +
                                   "transaction</em></td></tr>");
    }

    if (nextTr) {
        this.$timelineTable.append(utils.toTr(
            "Next Transaction",
            nextTr.id,
            nextTr.budget_category,
            nextTr.name,
            utils.toMoney(nextTr.amount),
            utils.date(nextTr.date)
        ));
    } else {
        this.$timelineTable.append("<tr><td>Next Transaction</td>" +
                                   "<td colspan=\"5\"><em>At last " +
                                   "transaction</em></td></tr>");
    }

    this.$trDate.text(utils.date(this.now));
    this.$transactionsTable.append(this.budget_categories.map(this._bcMap, this));
    finance.updateTotal("transactions", this.total, this.now);
};
