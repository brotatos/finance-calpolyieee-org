"use strict";

finance.modules.push("equity");
finance.equity = {};

finance.equity.setup = function () {
    var self = this;

    this.total = 0;
    this.$equity = $("#equity");
    this.$eqTable = this.$equity.find("tbody");
    
    $.get(finance.url + "/kits/", utils.orAlert(function (data) {
        self.kits = data.kits;
        self.show();
    }));
};

finance.equity._mapKit = function (kit) {
    var value = kit.stock * kit.price;

    this.total += value;
    
    return utils.toTr(kit.name, utils.toMoney(kit.price),
        kit.stock, utils.toMoney(value));
};

finance.equity.show = function () {
    this.total = 0;
    this.$eqTable.empty().append(this.kits.map(this._mapKit, this));
    finance.updateTotal("equity", this.total);
};
