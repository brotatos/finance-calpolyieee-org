"use strict";

finance.modules.push("reimbursements");
finance.reimbursements = {};

finance.reimbursements.setup = function () {
    var self = this, url = finance.url + "/reimbursements/";

    this.$reimbursements = $("#reimbursements");
    this.$rrStatus = this.$reimbursements.find("span:nth-child(3) > select");
    this.$rrType = this.$reimbursements.find("span:nth-child(2) > select");
    this.$rrTable = this.$reimbursements.find("tbody");
    this.editModal = new utils.ModalAlert();

    this.$rrStatus.change(function () { self.show(); });

    $("body").append(this.editModal.$frame);

    this.$rrStatus.val("PENDING");

    $.get(url, {sort: "-id"}, utils.orAlert(function (data) {
        self.reimbursements = data.reimbursements;
        self.show();
    }));
};

finance.reimbursements._mapReimbursement = function (burse) {
    var self = this, $edit = utils.btn("Edit").addClass("btn-sm");
    var $delete = utils.btn("Delete", "danger").addClass("btn-sm");
    var $confirm = utils.btn("Confirm", "success");
    var message = "delete this reimbursement request?";
    var url = finance.url + "/reimbursements/" + burse.id + "/";
    var statusField = utils.Form.gen.select("Status");
    var typeField = utils.Form.gen.select("Type"), editForm;

    statusField.fill(["PENDING", "APPROVED", "COMPLETED"]);
    typeField.fill(["REGULAR", "CASHBOX"]);

    editForm = new utils.Form({
        amount: utils.Form.gen.money("Amount"),
        status: statusField,
        type: typeField,
        comment: utils.Form.gen.textarea("Comment", 2)
    }, {
        url: url,
        edit: true,
        populate: burse,
        successAction: function (data, json) {
            /*This isn't enough, we have to set the changed values as well*/
            self.show();
        }
    });

    $edit.click(function () {
        self.editModal.render({
            title: "Edit Reimbursement Request",
            body: editForm.render(),
            cancel: "OK",
        });
        self.editModal.show();
    });

    $delete.click(utils.doubleCheck(message, function () {
        $.delete_(finance.url + url, utils.orAlert(function () {
            utils.info({msg: "Request deleted.", dismiss: true});
            self.reimbursements.splice(self.reimbursements.indexOf(burse), 1);
            self.show();
        }));
    }));

    return utils.toTr(
        burse.id,
        utils.toMoney(burse.amount),
        utils.date(burse.date_requested),
        burse.date_approved && utils.date(burse.date_approved) || "<em>n/a</em>",
        burse.requester_name,
        burse.comment,
        burse.status,
        burse.type,
        $("<div>").append($edit, $delete)
    );
};

finance.reimbursements.show = function () {
    var status = this.$rrStatus.val();

    this.$rrTable.empty().append(
        this.reimbursements.filter(function (burse) {
            return status !== "NONE" ? burse.status === status : true;
        }).map(this._mapReimbursement, this)
    );
};
